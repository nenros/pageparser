#**APP IS NOT COMPATIBLE WITH JAVA 13**

## How to run it
1. `./gradlew bootJar`
2. `java -jar ./build/libs/app-0.0.1-SNAPSHOT.jar  "<page address>"`

## How to run tests
1 `./gradlew check`

## Trade offs
Probably using spring boot for that app is overhelm, but I've choose it for easy configuration(which wasn't needed at all). 


## Things for future
* for sure it can be optimized
* saving to file
* parent/child page
* more unit tests
* maybe better multithreading(but prbably current solution will work for many users)
* adding dockerfile

## Requirements
* Java 11
* Gradle
