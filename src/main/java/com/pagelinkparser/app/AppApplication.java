package com.pagelinkparser.app;

import com.pagelinkparser.app.services.PageParserAndPrinter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@Slf4j
@SpringBootApplication
public class AppApplication implements CommandLineRunner {

    private PageParserAndPrinter pageParserAndPrinter;

    public AppApplication(PageParserAndPrinter pageParserAndPrinter) {
        this.pageParserAndPrinter = pageParserAndPrinter;
    }


    public static void main(String[] args) {
        new SpringApplicationBuilder(AppApplication.class)
                .web(WebApplicationType.NONE)
                .run(args);
    }

    @Override
    public void run(String... args) {
        if (args.length == 0) {
            log.error("Please pass page to parse");
            return;
        }
        pageParserAndPrinter.run(args[0]);
    }


}
