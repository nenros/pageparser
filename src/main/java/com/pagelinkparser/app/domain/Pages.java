package com.pagelinkparser.app.domain;

import lombok.Getter;

import java.util.HashMap;

public class Pages {
    @Getter
    HashMap<String, Page> pageList;

    public Pages() {
        this.pageList = new HashMap<>();
    }

    public void addNewPage(Page page) {
        this.pageList.put(page.getPageAddress(), page);
    }

    public boolean pageAdded(String pageAddress) {
        return this.pageList.containsKey(pageAddress);
    }
}
