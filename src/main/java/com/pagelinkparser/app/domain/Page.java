package com.pagelinkparser.app.domain;

import com.pagelinkparser.app.services.Link;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Getter
public class Page {
    @NonNull
    private final String pageAddress;
    private final List<Link> pageLinks;

    public Page(String pageAddress) {
        this.pageAddress = pageAddress;
        this.pageLinks = new ArrayList<>();
    }

    public void addPageLink(Link link) {
        this.pageLinks.add(link);
    }

    public void addPageLinks(List<Link> links) {
        links.forEach(this::addPageLink);
    }

    public List<Link> getInternalLinks() {
        return this.pageLinks.stream()
                .filter(Link::getInternal)
                .collect(toList());
    }
}
