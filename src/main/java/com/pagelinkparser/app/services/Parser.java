package com.pagelinkparser.app.services;

import com.pagelinkparser.app.domain.Page;
import com.pagelinkparser.app.domain.Pages;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
@Slf4j
public class Parser {

    Pages pages;
    String baseDomain;

    public Pages parsePagesForDomain(String baseDomain) {
        this.pages = new Pages();
        this.baseDomain = baseDomain;
        parsePage(baseDomain).ifPresent(page -> {
            this.pages.addNewPage(page);
            log.info("Parsing page: {}", baseDomain);
            parseSubPages(page);
        });

        return this.pages;
    }

    void parseSubPages(Page page) {
        page
                .getInternalLinks()
                .stream()
                .filter((link) -> link.getInternal() && !this.pages.pageAdded(link.getLink()))
                .forEach(link -> {
                    log.info("Parsing  subpage: {}", link.getLink());
                    parsePage(link.getLink()).ifPresent((subPage) -> {
                        this.pages.addNewPage(subPage);
                        parseSubPages(subPage);
                    });
                });
    }

    Optional<Page> parsePage(String pageAddress) {
        try {

            Page page = new Page(pageAddress);

            log.debug("Fetching page: {}", page.getPageAddress());

            Document doc = Jsoup.connect(page.getPageAddress()).get();

            page.addPageLinks(prepareLinkByTag(doc, "a", "href", pageAddress));
            page.addPageLinks(prepareLinkByTag(doc, "img", "src", pageAddress));

            log.debug("Actual page: {}", page.getPageAddress());


            return Optional.of(page);
        } catch (Exception e) {
            log.error("Error while parsing page: {}", pageAddress);
            return Optional.empty();
        }
    }

    private List<Link> prepareLinkByTag(Document doc, String tag, String linkSource, String page) {
        return doc.getElementsByTag(tag)
                .stream()
                .map(element -> element.attr(linkSource))
                .filter(link -> !link.equals(page) && !link.startsWith("#"))
                .distinct()
                .map((link) -> Link.builder()
                        .link(link)
                        .internal(link.startsWith(this.baseDomain) || link.startsWith("/"))
                        .image(tag.equals("img"))
                        .build()
                ).collect(toList());
    }
}
