package com.pagelinkparser.app.services;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Link {
    String link;
    Boolean internal;
    Boolean image;
}
