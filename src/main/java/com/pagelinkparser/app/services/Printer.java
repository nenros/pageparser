package com.pagelinkparser.app.services;

import com.pagelinkparser.app.domain.Page;
import com.pagelinkparser.app.domain.Pages;
import org.springframework.stereotype.Service;

@Service
public class Printer {
    public void printLinkList(Pages pages) {
        pages.getPageList().forEach(this::printPage);
    }

    private void printPage(String s, Page page) {

        System.out.println("-".repeat(10));
        System.out.println("Page: " + s);
        System.out.println("|");
        page.getPageLinks().forEach(link -> System.out.println("|" + getLinkPrefix(link) + " " + link.getLink()));
    }

    private String getLinkPrefix(Link link) {
        return (link.getInternal() ? "I" : "E") +
                (link.getImage() ? "I" : "L");
    }
}
