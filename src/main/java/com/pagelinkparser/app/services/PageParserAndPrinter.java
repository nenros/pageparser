package com.pagelinkparser.app.services;

import com.pagelinkparser.app.domain.Pages;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PageParserAndPrinter {

    private final Parser parser;
    private final Printer printer;


    public PageParserAndPrinter(Parser parser, Printer printer) {
        this.parser = parser;
        this.printer = printer;
    }

    public void run(String page) {
        log.info("Page to parse: {}", page);
        log.debug("Start parsing");
        Pages pages = parser.parsePagesForDomain(page);
        log.info("Found pages: {}", pages.getPageList().size());
        printer.printLinkList(pages);
        log.info("Pages printed");
    }
}
