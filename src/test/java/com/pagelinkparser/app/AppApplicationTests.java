package com.pagelinkparser.app;

import com.pagelinkparser.app.services.PageParserAndPrinter;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class AppApplicationTests {

	@MockBean
	PageParserAndPrinter pageParserAndPrinter;

	@Test
	void contextLoads() {
	}

}
