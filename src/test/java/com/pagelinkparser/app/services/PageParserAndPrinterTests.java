package com.pagelinkparser.app.services;

import com.pagelinkparser.app.domain.Page;
import com.pagelinkparser.app.domain.Pages;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.HashMap;
import java.util.Optional;

import static org.mockito.Mockito.*;

@SpringBootTest
public class PageParserAndPrinterTests {
    private final String pageAddress = "http://test-page.com";
    private final Page page = new Page(pageAddress);
    private Pages pages;
    @Autowired
    PageParserAndPrinter pageParserAndPrinter;
    @MockBean
    Parser parser;
    @MockBean
    Printer printer;

    @BeforeEach
    private void setUp() {
        HashMap<String, Page> pageList = new HashMap<>();
        pageList.put(pageAddress, page);
        pages = new Pages();
    }

    @Test
    public void parserReturnDataAndCallPrinter() {
        when(parser.parsePage(pageAddress)).thenReturn(Optional.of(pages));
        doNothing().when(printer).printLinkList(pages);

        pageParserAndPrinter.run(pageAddress);

        verify(parser, times(1)).parsePage(pageAddress);
        verify(printer, times(1)).printLinkList(pages);
    }

    @Test
    public void dontCallPrinterIfParserEmpty() {
        when(parser.parsePage(pageAddress)).thenReturn(Optional.empty());
        doNothing().when(printer).printLinkList(pages);

        pageParserAndPrinter.run(pageAddress);

        verify(parser, times(1)).parsePage(pageAddress);
        verify(printer, times(0)).printLinkList(pages);
    }

}
